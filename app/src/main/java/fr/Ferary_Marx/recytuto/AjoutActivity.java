package fr.Ferary_Marx.recytuto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class AjoutActivity extends AppCompatActivity {

    private RadioGroup radioGroup;
    private RadioButton radioButton;
    private Button btnDisplay;
    private EditText tfName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajout);
        addListenerOnButton();
    }

    public void addListenerOnButton() {
        radioGroup = (RadioGroup) findViewById(R.id.radioTag);
        btnDisplay = (Button) findViewById(R.id.button);
        tfName = (EditText) findViewById(R.id.tfName);

        btnDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = radioGroup.getCheckedRadioButtonId();
                radioButton = (RadioButton) findViewById(selectedId);
                TodoItem.Tags tag = TodoItem.getTagFor(radioButton.getText().toString());

                TodoItem item = new TodoItem(tag, tfName.getText().toString());
                TodoDbHelper.addItem(item, getBaseContext());
                Intent dbmanager = new Intent(AjoutActivity.this, MainActivity.class);
                startActivity(dbmanager);
            }
        });
    }
}
